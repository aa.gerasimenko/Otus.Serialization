﻿using System;
using BenchmarkDotNet.Running;

namespace Otus.Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            //new TimerDemo().Show();

            Console.WriteLine("Hello World!");
            var summary = BenchmarkRunner.Run<TimerDemo>();
            Console.WriteLine("Good bye!");
        }
    }
}
